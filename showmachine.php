<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Consulta de  VM</title>
    <style>
      body
      {
        background-image: url(all-of-those-images/interf/logoazul.png);
        background-repeat: no-repeat;
        background-size: 260px 70px;
        background-position: 96% 25px;
      }
      th
      {
        font-size: 20px;
        font-weight: bold;
        border: 1px flat #000033;
      }
      td,th
      {
        text-align: center;
      }
      table
      {
        width: 95%;
      }
      .evilbtn
      {
        display: inline;
        background-color: #F7DC6F;
        border:  double #FCF3CF;
        font-size: 15px;
        font-weight: bold;
        float: left;
        margin-right: 5%;
        cursor: default;
        border-radius: 50%;
        height: 60px;
      }
      .containerOfRod
      {
        padding: 4px 4px;
        box-sizing: border-box;
        font-size: 16px;
        border:10px groove #616161;
        border-radius: 10px; 
      }
      .shad
      {
        font-size: 20px;
        font-weight: bold;
        border: 1px flat #000033;
      }
      .unselectable
      {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
    </style>
    <?php include 'dbc.php';?>
  </head>
  <body>
    <div class="containerOfRod" >
    <br><br>
    <div class="shad" align="center">
      <?php
        $conn = mysqli_connect($host,$user,$pass,$db);
        $sql="select SDQ from filtmachine where No=".$_GET['no']."  limit 1";
        $re = mysqli_query($conn,$sql);
        $h=mysqli_fetch_array($re);
        echo "Disco compartido (spec):<input type=\"text\" value=\"".$h['SDQ']."\" name=\"sdis\" autocomplete=\"off\" disabled >";
      ?>
    </div>
    <br><br>
      <table>
        <tr>
          <th>No:</th>
          <th>vCPUs:</th>
          <th>Memoria (gb):</th>
          <th>Disco (gb):</th>
          <th>SO:</th>
          <th>Base De Datos:</th>
        </tr>
        <?php
          $sql="select No,CPU,Mem,SD,SO,DB from filtmachine where No=".$_GET['no'];
          $some=array('No','CPU','Mem','SD','SO','DB');
          $re = mysqli_query($conn,$sql);
          if(!$re)
            echo "Conexion con BD fallida";
          else
          {
            while($row = mysqli_fetch_array($re))
            {
              echo "<tr>";
              for($i = 0;$i < 6; $i++)
                  echo "<td>".$row[$some[$i]]."</td>";
              echo "</tr>";
            }
          }
          mysqli_close($conn);
        ?>
      </table>
      <button type="button" class="evilbtn">Tecnologias Cloud</button>
    <br><br><br><br>
    </div>
  </body>
</html>