<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Solicitudes</title>
    <style>
      body
      {
        background-image: url(all-of-those-images/interf/logoazul.png);
        background-repeat: no-repeat;
        background-size: 260px 70px;
        background-position: 96% 25px;
      }
      #FR,#Sol,#Ger,#Dir,#PN,#RE,#Status,#Pro,#ASol,#Des,#Pvd,#SVM,#SVCPU,#SRAM,#SD,#Com,#AD,#InfD,#EVM,#EVCPU,#ERAM,#ED,#FESO,#FEUF,#Com2
      {
        width:120px;
        float:right;
        margin-right:40px;
      }
      button,input[type=submit]
      {
        background-color: #D6EAF8;
        padding: 4px 4px;
        border: outset #ABB2B9;
        cursor: pointer;
        font-size: 15px;
        font-weight: bold;
        box-shadow: 2px 3px 10px #000033;
      }
      .evilbtn
      {
        display: inline;
        background-color: #F7DC6F;
        border:  double #FCF3CF;
        font-size: 15px;
        font-weight: bold;
        float: right;
        margin-right: 5%;
        cursor: default;
        border-radius: 50%;
        height: 60px;
      }
      .container
      {
        padding: 4px 4px;
        box-sizing: border-box;
        font-size: 16px;
        border:10px groove #616161;
        border-radius: 25px;
      }
      .unselectable
      {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      .line1 , .line2 , .line3
      {
        float: left;
        margin-left:5px;
        display:inline;
      }
      .line1
      {
        text-shadow: 1px 1px 5px ##151f6b;
        #background: #d0cbd6;
        width: 28%;
      }
      .line2
      {
        width: 33%;
      }
      .line3
      {
        text-shadow: 1px 1px 5px #151f6b;
        #background: #d0cbd6;
        width: 33%;
      }
      .container2
      {
        padding: 6px ;
        display: inline-block;
        width: 100%;
      }
    </style>
    <?php 
      include 'dbc.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
      if($_POST['sola']=="")
        $_POST['sola']=1;
      $vm=array('cpu' =>0,'mem' =>0,'sd'=>0,'sdq'=>0);
      $sql="select CPU,Mem,SD,SDQ from filtmachine where No=".$_POST['sola'];
      $re = mysqli_query($conn,$sql);
      if(!$re)
        echo "Conexion con BD fallida".mysqli_error();
      else
        {
          $vmpeople=0;
          while($row0 = mysqli_fetch_array($re))
           {
             $vmpeople++;
             $vm['cpu'] += $row0['CPU'];
             $vm['mem'] += $row0['Mem'];
             $vm['sd'] += $row0['SD'];
             $vm['sdq'] += $row0['SDQ'];
           }
        }
      $sql="select * from filtro where FR2=".$_POST['sola'];
      $re = mysqli_query($conn,$sql);
      if(!$re)
        echo "Conexion con BD fallida".mysqli_error();
      else
        {
          $row = mysqli_fetch_array($re);
          $thatData = array('FR' => $row['FR'],'Sol' => $row['Sol'],'Ger' => $row['Ger'],'Dir' => $row['Dir'],'PN' => $row['PN'],'RE' => '','Status' => $row['Status'],'Pro' => $row['Pro'],'ASol' => $row['ASol'],'Des' => $row['DP'],'Pvd' => $row['PP'],'SVM' => $vmpeople,'SVCPU'=> $vm['cpu'] ,'SRAM'=> $vm['mem'] ,'SD'=> $vm['sd'] ,'Com' =>$row['Com'],'AD' =>'','InfD'=>'','EVM'=>0,'EVCPU'=>0,'ERAM'=>0,'ED'=>0,'FESO'=>'','FEUF'=>'','Com2'=>'');
        }
    ?>
  </head>
  <body>
    <div class="container" align="center">
    <br><br>
    <form method='post' action='judement.php' >
    <div class="container2" align="center">
        <input type="submit" value="Aprobar Proyecto" name="thisisnice" id="thisisnice" >
        <input type="submit" value="Rechazar Proyecto" name="mercifullno" id="mercifullno">
        <input type="submit" value="Rechazar y eliminar" name="killandgo" id="killandgo">
      </div>
      <br>
          No es necesario llenar todos los campos para aprobar o rechazar un proyecto 
    <br><br>
      <div class="line1">
        Fecha de recepcion : <input type="text" value="<?php echo $thatData['FR']; ?>" disabled>
        <input type="hidden" id="FR" name="FR" value="<?php echo $thatData['FR']; ?>" > 
        </div>
      <div class="line2">
        Solicitante : <input type="text" value="<?php echo $thatData['Sol']; ?>" disabled>
        <input type="hidden"  id="Sol" name="Sol" value="<?php echo $thatData['Sol']; ?>" >
        <input type="hidden"  id="sola" name="sola" value="<?php echo $_POST['sola']; ?>" >
        </div>
      <div class="line3">
        Gerencia : <input type="text"  value="<?php echo $thatData['Ger']; ?>" disabled>
        <input type="hidden" id="Ger" name="Ger" value="<?php echo $thatData['Ger']; ?>" >
        </div>
      <br><br>
      <div class="line1">
        Direccion : <input type="text"  value="<?php echo $thatData['Dir']; ?>" disabled>
        <input type="hidden" id="Dir" name="Dir" value="<?php echo $thatData['Dir']; ?>" >
        </div>
      <div class="line2">
        Requerimiento : <input type="text"  value="<?php echo $thatData['PN']; ?>" disabled>
        <input type="hidden" id="PN" name="PN" value="<?php echo $thatData['PN']; ?>" >
        </div>
      <div class="line3">
        Responsable Entrega :  <select name="RE" id="RE" >
        <option <?php if($thatData['RE'] == ''){echo("selected");}?> value=""></option>
        <?php 
          $re = mysqli_query($conn,"select nombre from infteam");
          if(! $re)
            echo "<option value=\"Pendiente\">Pendiente</option> ";
          else
            while($row2 = mysqli_fetch_array($re))
            {
              $o ="<option ";
              if($thatData['RE'] == $row2['nombre'])
                $o.=" selected ";
              $o.="value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
              echo $o;
            }
        ?>
        </select>
        </div>
      <br><br>
      <div class="line1">
        Status : <input type="text" value="<?php echo $thatData['Status']; ?>" disabled>
        <input type="hidden"  id="Status" name="Status" value="<?php echo $thatData['Status']; ?>" >
        </div>
      <div class="line2">
        Proyecto : <input type="text" value="<?php echo $thatData['Pro']; ?>" disabled>
        <input type="hidden"  id="Pro" name="Pro" value="<?php echo $thatData['Pro']; ?>" >
        </div>
      <div class="line3">
        Ambiente Solicitado : <input type="text"  value="<?php echo $thatData['ASol']; ?>" disabled>
        <input type="hidden" id="ASol" name="ASol" value="<?php echo $thatData['ASol']; ?>" >
        </div>
      <br><br>
      <div class="line1">
        Descripcion : <input type="text" value="<?php echo $thatData['Des']; ?>" disabled>
        <input type="hidden"  id="Des" name="Des" value="<?php echo $thatData['Des']; ?>" >
        </div>
      <div class="line2">
        Proveedor : <input type="text" value="<?php echo $thatData['Pvd']; ?>" disabled>
        <input type="hidden" id="Pvd" name="Pvd"  value="<?php echo $thatData['Pvd']; ?>" >
        </div>
      <div class="line3">
        Total VMs : <input type="text"  value="<?php echo $thatData['SVM']; ?>" disabled>
        <input type="hidden" id="SVM" name="SVM" value="<?php echo $thatData['SVM']; ?>" > 
        </div>
      <br><br>
      <div class="line1">
        Total vCPU : <input type="text" value="<?php echo $thatData['SVCPU']; ?>" disabled>
        <input type="hidden" id="SVCPU" name="SVCPU"  value="<?php echo $thatData['SVCPU']; ?>" >
        </div>
      <div class="line2">
        Total RAM : <input type="text" value="<?php echo $thatData['SRAM']; ?>" disabled>
        <input type="hidden"  id="SRAM" name="SRAM" value="<?php echo $thatData['SRAM']; ?>" >
        </div>
      <div class="line3">
        Total Disco : <input type="text" value="<?php echo $thatData['SD']; ?>" disabled>
        <input type="hidden"  id="SD" name="SD" value="<?php echo $thatData['SD']; ?>" >
        </div>
      <br><br>
      <div class="line1">
        Comentarios soli : <input type="text" value="<?php echo $thatData['Com']; ?>" disabled> 
        <input type="hidden" id="Com" name="Com"  value="<?php echo $thatData['Com']; ?>" >
        </div>
      <div class="line2">
        Ambiente definido :  <select name="AD" id="AD" >
        <option <?php if($thatData['AD'] == ''){echo("selected");}?> value=""></option>
        <?php 
          $re = mysqli_query($conn,"select nombre from ambdef");
          if(! $re)
            echo "<option value=\"Pendiente\">Pendiente</option> ";
          else
            while($row2 = mysqli_fetch_array($re))
            {
              $o ="<option ";
              if($thatData['AD'] == $row2['nombre'])
                $o.=" selected ";
              $o.="value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
              echo $o;
            }
        ?>
        </select>
        </div>
      <div class="line3">
        Infra def para alojar :  <select name="InfD" id="InfD" >
        <option <?php if($thatData['InfD'] == ''){echo("selected");}?> value=""></option>
        <?php 
          $re = mysqli_query($conn,"select nombre from infdef");
          if(! $re)
            echo "<option value=\"Pendiente\">Pendiente</option> ";
          else
            while($row2 = mysqli_fetch_array($re))
            {
              $o ="<option ";
              if($thatData['InfD'] == $row2['nombre'])
                $o.=" selected ";
              $o.="value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
              echo $o;
            }
          mysqli_close($conn);
        ?>
        </select>
        </div>
      <br><br>
      <div class="line1">
        VMs Entregadas :  <input type="number" name="EVM" id="EVM" value="<?php echo $thatData['EVM']; ?>" size="5" autocomplete="off"  maxlength="3" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
        </div>
      <div class="line2">
        Total CPU Entregado :  <input type="number" name="EVCPU" id="EVCPU" value="<?php echo $thatData['EVCPU']; ?>" size="5" autocomplete="off"  maxlength="3" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
        </div>
      <div class="line3">
        Total RAM Entregado :  <input type="number" name="ERAM" id="ERAM" value="<?php echo $thatData['ERAM']; ?>" size="5" autocomplete="off"  maxlength="3" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
        </div>
      <br><br>
      <div class="line1">
        Disco Total Entregado :  <input type="number" name="ED" id="ED" value="<?php echo $thatData['ED']; ?>" size="5" autocomplete="off"  maxlength="3" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
        </div>
      <div class="line2">
        Fecha de entrega a SO:  <input type="date" name="FESO" id="FESO" value="<?php echo $thatData['FESO']; ?>">
        </div>
      <div class="line3">
        Fecha de entrega a Usuario:  <input type="date" name="FEUF" id="FEUF" value="<?php echo $thatData['FEUF']; ?>">
        </div>
      <br><br>
      <div class="line1">
        Comentarios Infra : <input type="text" id="Com2" name="Com2" value="<?php echo $thatData['Com2']; ?>" autocomplete="off">
        </div>
      <div class="line2">
        <?php echo "<script>var solicitud=".$_POST['sola']."</script>"?>
        <input type="button" id="bt" onclick="window.open('showmachine.php?no='+solicitud,'','menubar=0,titlebar=0,width=1280,height=400,resizable=0,left=40px,top=250px')" value="Mostrar Maquinas Virtuales" />
        </div>
      <div class="line3">
        <?php echo "<script>var solicitud=".$_POST['sola']."</script>"?>
        <input type="button" id="Dia" onclick="window.open('showthatmap.php?no='+solicitud,'','menubar=0,titlebar=0,width=650,height=580,resizable=0,left=40px,top=50px')" value="Mostrar Diagrama Infraestructura" />
        </div>
      <br><br>
    </form>
    <?php echo "<form action='http://".$inside."'>"?><input type="submit" value="Regresar" ></form>
    <button type="button" class="evilbtn">Tecnologias Cloud</button>
    <br><br><br><br><br>  
    </div>
  </body>
</html>